/*  Функции позволяют избежать повторного выполнения одной и той же последовательности команд, и дают возможность уменьшить количество кода, так как их можно вызывать неограниченное количество раз и передавать результаты ее выполнения в любое место кода. Функция получая аргументы присваивает их своим параметрам, тем самым становится универсальной, то есть имея вложенный в себя алгоритм расчета и принимая различные аргументы возвращает результат, расчитанный на основе полученной информации. Это и позволяет создать одну функцию для однотипного расчета на основаниеи различных входных данных.
*/

const dataSet = gettingInfo();
const checkDataSet = checkData(dataSet[0], dataSet[1], dataSet[2]);
calculation(checkDataSet[0], checkDataSet[1], checkDataSet[2]);
console.log(calculation(checkDataSet[0], checkDataSet[1], checkDataSet[2]));

function gettingInfo() {                          //getting data
  const firstNumber = prompt("Enter first number");
  const sign = prompt("Enter arithmetic operation");
  const secondNumber = prompt("Enter second number");
    return [firstNumber, sign, secondNumber];
}

function checkData(a, b, c) {                      //data validation
    while (a === "" || c === "") {                 //verification of not entered data
        a = prompt("Are you sure you entered the correct number?", a);
        c = prompt("Are you sure you entered the correct number?", c);
  }

  let aFault = a;   //creating a variable for default values
  let cFault = c;   //creating a variable for default values
  a = Number(a);
  c = Number(c);

  while (Number.isNaN(a) || Number.isNaN(c)) {      //number format check
    a = +prompt("Are you sure you entered the correct number?", aFault);
    c = +prompt("Are you sure you entered the correct number?", cFault);
  }

  while (b !== "+" && b !== "-" && b !== "*" && b !== "/") {  //check the format of an arithmetic operation
    b = prompt("Are you sure you entered the correct arithmetic operation ?", b);
  }

  return [a, b, c];
}

function calculation(num1, sign, num2) {
  switch (sign) {
    case "*":
      return num1 * num2;
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "/":
      return num1 / num2;
  }
  
}
